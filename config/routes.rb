Rails.application.routes.draw do

  resources :convocations do
    member do 
      get  'cargar-postulaciones'    => 'convocations#cargar_postulaciones',              as: 'carga_postulaciones'
      post 'importar-postulaciones' => 'convocations#importar_postulaciones',            as: 'import_applications'
      get  'exportar-prepostulantes' => 'convocations#exportar_prepostulantes',           as: 'exportar_prepostulantes'
      get  'ipe/html'                => 'convocations#html_prepostulantes_evaluados',     as: 'html_prepostulantes_evaluados'
      post 'ipe'                     => 'convocations#importar_prepostulantes_evaluados', as: 'importar_prepostulantes_evaluados'
      get "make"       => "convocations#make_draw", as: 'make_draw'
      get "results"    => "convocations#results"
      post 'sendmails' => 'convocations#send_mails'  
      post 'sendorganizationsmails' => 'convocations#send_mails_to_organizations'
    end
    collection do 
      get 'findapplications' => 'convocations#find_applications'
      post 'make'            => 'convocations#make_draw!'
    end
  end

  devise_for :users
  root to: "dashboard#index"

  get 'beneficiarios/:id' => 'applications#carta_beneficiario', as: :carta_beneficiario

  get 'convocations/:id/organization/:id_empresa' => 'convocations#carta_empresa', as: :carta_empresa

  resources :applications do
    collection do
      get 'full' => 'applications#full', as: 'full_application'
    end
  end

  resources :organizations

  resources :applicants do
    collection do
      post :import
    end
  end

  resources :departaments

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'


  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end

# == Schema Information
#
# Table name: applicants
#
#  id                           :integer          not null, primary key
#  apellidos                    :string(255)
#  nombres                      :string(255)
#  sexo                         :string(1)
#  cuil                         :string(255)
#  dni                          :string(255)
#  fecha_nacimiento             :date
#  telefono                     :string(30)
#  estado_civil                 :string(20)
#  hijos                        :integer
#  domicilio                    :string(255)
#  localidad                    :string(255)
#  departament_id               :integer
#  codigo_postal                :string(10)
#  escolaridad_nivel            :string(255)
#  escolaridad_cursa            :boolean
#  escolaridad_finalizada       :boolean
#  escolaridad_quiere_finalizar :boolean
#  fecha_solicitud              :date
#  email                        :string(255)
#  habilitado                   :boolean
#  contratado                   :boolean
#  created_at                   :datetime
#  updated_at                   :datetime
#

require 'test_helper'

class ApplicantTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end

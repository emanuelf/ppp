namespace :db do
  namespace :seed do
    task :single => :environment do
      filename = File.join(Rails.root, 'db', 'seeds', "#{ENV['SEED']}.seeds.rb")
      puts "Seeding #{filename}..."
      load(filename) if File.exist?(filename)
    end
  end
end
# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170228005946) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "applicants", force: true do |t|
    t.string   "apellidos"
    t.string   "nombres"
    t.string   "sexo",                         limit: 1
    t.string   "cuil"
    t.string   "dni"
    t.date     "fecha_nacimiento"
    t.string   "telefono",                     limit: 30
    t.string   "estado_civil",                 limit: 20
    t.integer  "hijos"
    t.string   "domicilio"
    t.string   "localidad"
    t.integer  "departament_id"
    t.string   "codigo_postal",                limit: 10
    t.string   "escolaridad_nivel"
    t.boolean  "escolaridad_cursa"
    t.boolean  "escolaridad_finalizada"
    t.boolean  "escolaridad_quiere_finalizar"
    t.date     "fecha_solicitud"
    t.string   "email"
    t.boolean  "habilitado"
    t.boolean  "contratado"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "applicants", ["departament_id"], name: "index_applicants_on_departament_id", using: :btree

  create_table "applications", force: true do |t|
    t.integer  "applicant_id"
    t.integer  "organization_id"
    t.string   "tipo_contratacion", limit: 30
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "estado",            limit: 20
    t.integer  "draw_id"
    t.integer  "convocation_id"
  end

  add_index "applications", ["applicant_id"], name: "index_applications_on_applicant_id", using: :btree
  add_index "applications", ["convocation_id"], name: "index_applications_on_convocation_id", using: :btree
  add_index "applications", ["draw_id"], name: "index_applications_on_draw_id", using: :btree
  add_index "applications", ["organization_id"], name: "index_applications_on_organization_id", using: :btree

  create_table "convocation_applicants", force: true do |t|
    t.integer  "convocation_id"
    t.integer  "applicant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "convocation_applicants", ["applicant_id"], name: "index_convocation_applicants_on_applicant_id", using: :btree
  add_index "convocation_applicants", ["convocation_id"], name: "index_convocation_applicants_on_convocation_id", using: :btree

  create_table "convocation_organizations", force: true do |t|
    t.integer  "convocation_id"
    t.integer  "organization_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "convocation_organizations", ["convocation_id"], name: "index_convocation_organizations_on_convocation_id", using: :btree
  add_index "convocation_organizations", ["organization_id"], name: "index_convocation_organizations_on_organization_id", using: :btree

  create_table "convocations", force: true do |t|
    t.date     "fecha_limite"
    t.date     "fecha_inicio"
    t.string   "nombre",              limit: 50
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "sorteo_realizado_el"
    t.string   "estado",              limit: 30
  end

  create_table "departaments", force: true do |t|
    t.string   "nombre"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "draws", force: true do |t|
    t.date     "fecha_realizacion"
    t.string   "nombre"
    t.boolean  "realizado"
    t.time     "realizado_el"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "electeds", force: true do |t|
    t.integer  "application_id"
    t.integer  "draw_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "electeds", ["application_id"], name: "index_electeds_on_application_id", using: :btree
  add_index "electeds", ["draw_id"], name: "index_electeds_on_draw_id", using: :btree

  create_table "organizations", force: true do |t|
    t.string   "cuit"
    t.string   "codigo_actividad",  limit: 15
    t.integer  "cantidad_personal"
    t.string   "calle"
    t.string   "nro"
    t.string   "localidad"
    t.string   "telefono"
    t.string   "fax"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "nombre"
    t.integer  "departament_id"
    t.string   "domicilio"
  end

  add_index "organizations", ["departament_id"], name: "index_organizations_on_departament_id", using: :btree

  create_table "unelecteds", force: true do |t|
    t.integer  "application_id"
    t.integer  "draw_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "unelecteds", ["application_id"], name: "index_unelecteds_on_application_id", using: :btree
  add_index "unelecteds", ["draw_id"], name: "index_unelecteds_on_draw_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end

class CreateConvocationOrganizations < ActiveRecord::Migration
  def change
    create_table :convocation_organizations do |t|
      t.belongs_to :convocation, index: true
      t.belongs_to :organization, index: true

      t.timestamps
    end
  end
end

class CreateDepartaments < ActiveRecord::Migration
  def change
    create_table :departaments do |t|
      t.string :nombre

      t.timestamps
    end
  end
end

class AddConvocationIdToApplications < ActiveRecord::Migration
  def change
    add_reference :applications, :convocation, index: true
  end
end

class AddDrawIdToApplications < ActiveRecord::Migration
  def change
    add_reference :applications, :draw, index: true
  end
end

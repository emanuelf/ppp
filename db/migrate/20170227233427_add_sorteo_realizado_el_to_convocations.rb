class AddSorteoRealizadoElToConvocations < ActiveRecord::Migration
  def change
    add_column :convocations, :sorteo_realizado_el, :date
  end
end

class CreateDraws < ActiveRecord::Migration
  def change
    create_table :draws do |t|
      t.date :fecha_realizacion
      t.string :nombre
      t.boolean :realizado
      t.time :realizado_el

      t.timestamps
    end
  end
end

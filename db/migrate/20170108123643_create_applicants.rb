class CreateApplicants < ActiveRecord::Migration
  def change
    create_table :applicants do |t|
      t.string :apellidos
      t.string :nombres
      t.string :sexo, limit: 1
      t.string :cuil
      t.string :dni
      t.date :fecha_nacimiento
      t.string :telefono, limit: 30
      t.string :estado_civil, limit: 20
      t.integer :hijos
      t.string :domicilio
      t.string :localidad
      t.belongs_to :departament, index: true
      t.string :codigo_postal, limit: 10
      t.string :escolaridad_nivel
      t.boolean :escolaridad_cursa
      t.boolean :escolaridad_finalizada
      t.boolean :escolaridad_quiere_finalizar
      t.date :fecha_solicitud
      t.string :email
      t.boolean :habilitado
      t.boolean :contratado

      t.timestamps
    end
  end
end

class AddEstadoToApplications < ActiveRecord::Migration
  def change
    add_column :applications, :estado, :string, limit: 20
  end
end

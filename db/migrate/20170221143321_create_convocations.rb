class CreateConvocations < ActiveRecord::Migration
  def change
    create_table :convocations do |t|
      t.date :fecha_limite
      t.date :fecha_inicio
      t.string :nombre, limit: 50

      t.timestamps
    end
  end
end

class AddEstadoToConvocations < ActiveRecord::Migration
  def change
    add_column :convocations, :estado, :string, default: 'Abierta', limit: 30
  end
end

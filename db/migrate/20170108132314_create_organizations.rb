class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.string :cuit
      t.string :codigo_actividad, limit: 15
      t.integer :cantidad_personal
      t.string :calle
      t.string :nro
      t.string :localidad
      t.string :telefono
      t.string :fax
      t.string :email

      t.timestamps
    end
  end
end

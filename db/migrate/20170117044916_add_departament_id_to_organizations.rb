class AddDepartamentIdToOrganizations < ActiveRecord::Migration
  def change
    add_reference :organizations, :departament, index: true
  end
end

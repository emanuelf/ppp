class CreateApplications < ActiveRecord::Migration
  def change
    create_table :applications do |t|
      t.belongs_to :applicant, index: true
      t.belongs_to :organization, index: true
      t.string :tipo_contratacion, limit: 30

      t.timestamps
    end
  end
end

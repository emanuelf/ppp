postulantes = Applicant.all
empresas    = Organization.all
tipos_contr = [Application::ENTRENAMIENTO, Application::CTI]
draw        = Draw.first
postulantes.each do |p|
  Application.create(
    organization: empresas.shuffle[0], 
    applicant: postulantes.shuffle[0],
    tipo_contratacion: tipos_contr.shuffle[0],
    draw: draw
  )
end
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(email: 'test@terciar.info', password: 'primerempleo', password_confirmation: 'primerempleo')

User.create(email: 'peteihatrabajo@empleo.corrientes.gov.ar', password: 'mbaepachamigo', password_confirmation: 'mbaepachamigo')

departaments = [{nombre: 'Bella Vista'},
         {nombre: 'Berón de Astrada'},
         {nombre: 'Capital'},
         {nombre: 'Concepción'},
         {nombre: 'Curuzú Cuatiá'},
         {nombre: 'Empedrado'},
         {nombre: 'Esquina'},
         {nombre: 'General Alvear'},
         {nombre: 'General Paz'},
         {nombre: 'Goya'},
         {nombre: 'Itatí'},
         {nombre: 'Ituzaingó'},
         {nombre: 'Lavalle'},
         {nombre: 'Mburucuyá'},
         {nombre: 'Mercedes'},
         {nombre: 'Monte Caseros'},
         {nombre: 'Paso de los Libres'},
         {nombre: 'Saladas'},
         {nombre: 'San Cosme'},
         {nombre: 'San Luis del Palmar'},
         {nombre: 'San Martín'},
         {nombre: 'San Miguel'},
         {nombre: 'San Roque'},
         {nombre: 'Santo Tomé'},
         {nombre: 'Sauce'}
     ]

departaments.each do |departament|
   Departament.create!(departament)
end
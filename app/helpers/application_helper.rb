module ApplicationHelper

  def class_active_if_controller(controller_name)
    if params[:controller].eql?(controller_name) 
      "active"
    end
  end

  def boolean_to_text (value)
    case value
    when true
      'Si'
    when false
      'No'
    when nil
      ' '
    else
      "N/C"
    end
  end

  def genere_to_text (value)
    case value
    when 'v'
      'Varón'
    when 'm'
      'Mujer'
    when 'V'
      'Varón'
    when 'M'
      'Mujer'
    else
      ''
    end
  end

  def badge_status(status)
    case status
    when true
      #verde
        @content = tag(:span, class: "badge alert-success")
    when false
      #default
        @content = tag(:span, class: "badge alert-danger")
    else
      tag(:span)
    end
  end

  def contratado_line(contratado)
    #Devolver la clase success para pintar la linea de verde
    if contratado
      @content = tag("tr", class: "success")
      else
      @content = tag("tr")
    end
  end

  def validated_text_field form, model, field, options = {}
    if model.errors[field].any?
      options[:class]        = options[:class] + " has-error"
      options["data-toggle"] = "tooltip"
      options["title"] = model.errors[field].join("\n")
    end
    form.text_field field, options
  end
end

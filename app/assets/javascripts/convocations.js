$(document).on('turbolinks:load', function() {    

  $("#file").change(function() {
    var nombreArchivo = $(this).val();
    if (nombreArchivo == "") {
      $("#importar").attr('disabled', true);
    } else {
      $("#importar").attr('disabled', false);
    }
    $("#nombre-archivo").html($(this).val().split(/(\\|\/)/g).pop());
  });

  $("#realizar_sorteo").click(function(e) {
    e.preventDefault();
    var b = confirm("¿Seguro de realizar el sorteo?")

    if (b) {
      var idConvocatoria = $("input[type='hidden']").val();
      $.post("/convocations/make", {id: idConvocatoria})
       .done(function(data){
          $("#postulantes").addClass('hidden');
          $("#adjudicados").removeClass('hidden');
          var tblAdjudicados = $("#adjudicados tbody");
          $.each(data.adjudicados, function(i, adjudicado) {
             tblAdjudicados.append(
               '<tr>' + 
                  '<td>' + adjudicado.nombre  + '<td>' + 
                  '<td>' + adjudicado.empresa + '<td>' + 
               '<tr>'                
             )
          });

          $("#rechazados").removeClass('hidden');
          var tblAdjudicados = $("#rechazados tbody");
          $.each(data.rechazados, function(i, rechazado) {
             tblAdjudicados.append(
               '<tr>' + 
                  '<td>' + rechazado.nombre  + '<td>' + 
                  '<td>' + rechazado.empresa + '<td>' + 
               '<tr>'                
             )
          });
          $('#realizar_sorteo').prop('disabled', true);
          var h1 = $('h1');
          h1.text("¡Sorteo realizado!");
          h1.css('color', 'green');
          $("h3").text(""); 
       }).fail(function (xhr) {
         alert(xhr.responseText);
       });    
    }
  });

  function mostrarApplications() {
    var idConvocatoria = $("input[type='hidden']").val();
    $.get("/convocations/findapplications", {id: idConvocatoria})
       .done(function(data) {
          $("#postulantes").removeClass('hidden');
          var table = $('#postulantes tbody');
          if (data.length < 1) { 
            table.html("");
            return false;
          }
          $.each(data, function(index, val) {
            table.append(
            '<tr>' + 
              '<td>'+ val.postulante.nombre +'</td>' + 
              '<td>'+ val.empresa.nombre +'</td>' + 
            '</tr>')
          });
          $('table').removeClass('hidden');
       }).fail(function () {
       });
  }

  mostrarApplications();

  $("#select_sorteo").change(function(e) {
    e.preventDefault();
    mostrarApplications();
  });

})
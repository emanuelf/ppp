$(document).on('turbolinks:load', function() {

  $("#mail_beneficiarios").click(function(){
    var convocationId = $("input[type='hidden']").val();
    $.post("/convocations/" + convocationId + "/sendmails")
      .done(function() {
        alert("Mails enviados");
      })
      .fail(function() {
        alert("Mails no enviados");
      });

  });

  $("#mail_empresas").click(function(){
    var convocationId = $("input[type='hidden']").val();
    $.post("/convocations/" + convocationId + "/sendorganizationsmails")
      .done(function() {
        alert("Mails enviados");
      })
      .fail(function() {
        alert("Mails no enviados");
      });

  });
});
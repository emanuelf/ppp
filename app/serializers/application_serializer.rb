# == Schema Information
#
# Table name: applications
#
#  id                :integer          not null, primary key
#  applicant_id      :integer
#  organization_id   :integer
#  tipo_contratacion :string(30)
#  created_at        :datetime
#  updated_at        :datetime
#  estado            :string(20)
#  draw_id           :integer
#  convocation_id    :integer
#

class ApplicationSerializer < ActiveModel::Serializer
  attributes :id, :postulante, :empresa

  def postulante
    {
      id: object.applicant.id,
      nombre: object.applicant.nombres
    }
  end

  def empresa
    {
      id: object.organization.id,
      nombre: object.organization.nombre
    }
  end
end

# == Schema Information
#
# Table name: convocations
#
#  id                  :integer          not null, primary key
#  fecha_limite        :date
#  fecha_inicio        :date
#  nombre              :string(50)
#  created_at          :datetime
#  updated_at          :datetime
#  sorteo_realizado_el :date
#

class ConvocationSerializer < ApplicationSerializer
  attributes :id, :fecha_limite, :fecha_inicio, :nombre
end

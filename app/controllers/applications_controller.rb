class ApplicationsController < ApplicationController

  before_action :set_application, only: [:show, :edit, :update, :destroy]
  skip_before_filter :authenticate_user!, only: [:carta_beneficiario]
 
  # GET /applications
  # GET /applications.json
  def index
    @applications = Application.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /applications/1
  # GET /applications/1.json
  def show
  end

  # GET /applications/new
  def new
    @application = Application.new
  end

  # GET /applications/full
  def full
    @application = Application.new
    @applicant = Applicant.new
    @organization = Organization.new
  end

  # GET /applications/1/edit
  def edit
  end

  def import
    if params[:file] == nil
      render :import_fail
    else
      Application.import(params[:file])
      render :import_finished
    end
  end

  def create_full
    #byebug
  begin
    Application.transaction do # un ActiveRecord
    
    # params['application']['applicant']
    # applicant, organization, application

    # Buscar si el candidato ya esta cargado.
    applicant_id = Application.find_applicant(params['application']['applicant']['cuil'])
    unless applicant_id
      @applicant = Applicant.new(applicant_params)
      @applicant.save
      applicant_id = @applicant.id      
    end
    #Chequear que no exista la organización.
    org_id = Application.find_organization(params['application']['organization']['cuit']) 
    unless org_id
      @organization = Organization.new(organization_params)
      # @organization.nombre = params['application']['organization']['nombre'] 
      # @organization.cuit = params['application']['organization']['cuit'] 
      # @organization.codigo_actividad = params['application']['organization']['codigo_actividad'] 
      # @organization.cantidad_personal = params['application']['organization']['cantidad_personal'] 
      # @organization.calle = params['application']['organization']['calle'] 
      # @organization.nro = params['application']['organization']['nro'] 
      # @organization.localidad = params['application']['organization']['localidad'] 
      # @organization.telefono = params['application']['organization']['telefono'] 
      # @organization.fax = params['application']['organization']['fax'] 
      # @organization.email = params['application']['organization']['email'] 
      # @organization.departament_id = params['application']['organization']['departament_id']
      @organization.save
      org_id = @organization.id
    end 

    params['application']['applicant']['id'] = applicant_id
    params['application']['organization']['id'] = org_id

    #byebug

    @application = Application.new #(create_application_params)
     @application.convocation_id = params['application']['convocation_id'].to_i 
     @application.applicant_id = params['application']['applicant']['id']
     @application.organization_id = params['application']['organization']['id']
     @application.tipo_contratacion = params['application']['application']['tipo_contratacion']
     @application.estado = params['application']['application']['estado']
    
    #byebug
    @application.save
    
    if @applicant.invalid? || @application.invalid? || @organization.invalid?
      raise "Faltan datos o son inválidos"
    end
    end
    #Código de éxito
    respond_to do |format|
      if @application.id
        format.html { redirect_to applications_path, notice: 'Postulación creada correctamente.' }
      else
        #format.html { render text: "Error al cargar la postulación. #{@application.errors.messages}", status: :ok }

        #format.html { redirect_to full_application_applications_path}
      end
    end
  rescue => e
    #raise ActiveRecord::Rollback #Lanzamos el rollback de nuevo a saco
    #Seguimos con las acciones que queramos, como notificar, etc.
    #byebug
    flash[:warning] = "Error al intentar Crear la postulación: #{e.message}"
    respond_to do |format|
      #format.html { redirect_to full_application_applications_path}
      @application = Application.new
      format.html { render :full}
    end

  end



  end


  # POST /applications
  # POST /applications.json
  def create
    if params['application']['applicant_id']
      @application = Application.new(application_params)

      respond_to do |format|
        if @application.save
          format.html { redirect_to @application, notice: 'Postulación creada correctamente.' }
          format.json { render :show, status: :created, location: @application }
        else
          format.html { render :new }
          format.json { render json: @application.errors, status: :unprocessable_entity }
        end
      end
    else
      create_full
    end
  end

  # PATCH/PUT /applications/1
  # PATCH/PUT /applications/1.json
  def update
    respond_to do |format|
      if @application.update(application_params)
        format.html { redirect_to @application, notice: 'Postulación actualizada correctamente.' }
        format.json { render :show, status: :ok, location: @application }
      else
        format.html { render :edit }
        format.json { render json: @application.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /applications/1
  # DELETE /applications/1.json
  def destroy
    @application.destroy
    respond_to do |format|
      format.html { redirect_to applications_url, notice: 'Postulación eliminada correctamente.' }
      format.json { head :no_content }
    end
  end

  def carta_beneficiario
    @application = Application.find(params[:id])
    if @application.adjudicado?
      render 'letters/beneficiario', layout: false, stutus: :ok
    else
      render text: 'Usted no ha sido adjudicado', status: :ok
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_application
      @application = Application.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def application_params
      params.require(:application).permit(:convocation_id, :applicant_id, :organization_id, :tipo_contratacion, :estado,
                                          applicant_attributes: [:apellidos, :nombres, :sexo, :cuil, :dni, :fecha_nacimiento, :telefono, :estado_civil, :hijos, :domicilio, :localidad, :departament_id, :codigo_postal, :escolaridad_nivel, :escolaridad_cursa, :escolaridad_finalizada, :escolaridad_quiere_finalizar, :fecha_solicitud, :email, :habilitado],
                                          organization_attributes:[:nombre, :cuit, :codigo_actividad, :cantidad_personal, :calle, :nro, :localidad, :telefono, :fax, :email, :departament_id],                                          
        )
    end

    def organization_params
      params['application']['organization'].permit(:nombre, :cuit, :codigo_actividad, :cantidad_personal, :calle, :nro, :localidad, :telefono, :fax, :email, :departament_id)
    end

     def applicant_params
      params['application']['applicant'].permit(:apellidos, :nombres, :sexo, :cuil, :dni, :fecha_nacimiento, :telefono, :estado_civil, :hijos, :domicilio, :localidad, :departament_id, :codigo_postal, :escolaridad_nivel, :escolaridad_cursa, :escolaridad_finalizada, :escolaridad_quiere_finalizar, :fecha_solicitud, :email, :habilitado, :contratado)
    end

    def create_application_params
      params['application']['application'].permit(:convocation_id, :applicant_id, :organization_id, :tipo_contratacion, :estado)
    end
end

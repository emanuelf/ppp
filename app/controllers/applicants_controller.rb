class ApplicantsController < ApplicationController
  before_action :set_applicant, only: [:show, :edit, :update, :destroy]

  def index
    @applicants = Applicant.all.order('apellidos').paginate(:page => params[:page], :per_page => 30)
  end

  def show
  end

  def new
    @applicant = Applicant.new
  end

  def edit
  end

  def import
    if params[:file] == nil
      render :import_fail
    else
      @excluidos_por_edad = Applicant.import(params[:file])
      render :import_finalizado
    end
  end

  def create
    @applicant = Applicant.new(applicant_params)

    respond_to do |format|
      if @applicant.save
        format.html { redirect_to @applicant, notice: 'Nuevo postulante creado correctamente.' }
        format.json { render :show, status: :created, location: @applicant }
      else
        format.html { render :new }
        format.json { render json: @applicant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /applicants/1
  # PATCH/PUT /applicants/1.json
  def update
    respond_to do |format|
      if @applicant.update(applicant_params)
        format.html { redirect_to @applicant, notice: 'Los datos del posutalante fueron ACTUALIZADOS correctamente.' }
        format.json { render :show, status: :ok, location: @applicant }
      else
        format.html { render :edit }
        format.json { render json: @applicant.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @applicant.destroy
    respond_to do |format|
      format.html { redirect_to applicants_url, notice: 'El postulante fue eliminado correctamente.' }
      format.json { head :no_content }
    end
  end

  private

    def set_applicant
      @applicant = Applicant.find(params[:id])
    end

    def applicant_params
      params.require(:applicant).permit(:apellidos, :nombres, :sexo, :cuil, :dni, :fecha_nacimiento, :telefono, :estado_civil, :hijos, :domicilio, :localidad, :departament_id, :codigo_postal, :escolaridad_nivel, :escolaridad_cursa, :escolaridad_finalizada, :escolaridad_quiere_finalizar, :fecha_solicitud, :email, :habilitado, :contratado)
    end
end

class ConvocationsController < ApplicationController

  skip_before_filter :authenticate_user!, only: [:send_mails, :send_mails_to_organizations, :make_draw, :make!, :carta_empresa]
  skip_before_filter :verify_authenticity_token, only: [:send_mails, :carta_beneficiario, :carta_empresa, :make!, :make_draw, :carta_beneficiario]

  before_action :set_convocation, only: [:show, 
                                         :edit, 
                                         :update, 
                                         :destroy, 
                                         :importar_postulaciones, 
                                         :importar_prepostulantes_evaluados,
                                         :cargar_postulaciones, 
                                         :exportar_prepostulantes, 
                                         :html_prepostulantes_evaluados,
                                         :make_draw!,
                                         :results,
                                         :send_mails,
                                         :find_applications,
                                         :make_draw, 
                                         :send_mails_to_organizations]


  respond_to :html

#--CRUD ACTIONS

  def index
    @convocations = Convocation.all.paginate(:page => params[:page], :per_page => 15)
    respond_with(@convocations)
  end

  def show
    respond_with(@convocation)
  end

  def new
    @convocation = Convocation.new
    respond_with(@convocation)
  end

  def edit
  end

  def create
    @convocation = Convocation.new(convocation_params)
    @convocation.save
    respond_with(@convocation)
  end

  def update
    @convocation.update(convocation_params)
    respond_with(@convocation)
  end

  def destroy
    @convocation.destroy
    respond_with(@convocation)
  end

#--CRUD ACTIONS

  def make_draw!
    if @convocation.sorteo_realizado?
      render json: "Sorteo ya realizado", status: :unprocessable_entity
    else
      @resultados = @convocation.sortear!
      render json: resultados_as_json, status: :ok
    end
  end

  def make_draw
    if @convocation.cerrada?
      @error   = true
      @message = "Esta convocatoria ya está cerrada"
    end
  end

  def results
    @resultados = @convocation.results
  end

  def send_mails
    if Rails.env.development?
      DrawMailer.mail_adjudicado(@convocation.where(estado: Application::ASIGNADO).first).deliver!
    end
    if Rails.env.production?
      @convocation.results[:adjudicados].each { |adj| DrawMailer.mail_adjudicado(adj).deliver! }
    end
    render json: {}, status: :ok
  end

  def send_mails_to_organizations
    byebug
    @convocation.organizations.each do |org|
      if @convocation.algun_beneficiario_asignado?(org)
        DrawMailer.mail_empresas(org, @convocation.adjudicados_of(org), @convocation).deliver!
      end
    end
    render json: {}, status: :ok
  end

  def find_applications
    render json: @convocation.applications, status: :ok
  end

  def cargar_postulaciones
    render :carga_postulaciones
  end

  def carta_empresa
    @org = Organization.find(params[:id_empresa])
    @convocation = Convocation.find(params[:id])
    render 'letters/empresa', layout: false, status: :ok
  end

  def importar_postulaciones
    if @convocation.cerrada?
      @error   = true
      @message = "La convocatoria ha sido cerrada ya"
      render :index and return
    end
    if params[:file]
      Application.import(params[:file], @convocation)
      render "applications/import_finished"
    else
      render "applicants/import_fail"
    end
  end

  def exportar_prepostulantes
    if @convocation.applications.empty?
      @error   = true 
      @message = "Las postulaciones no han sido cargadas aún"
      render :index and return
    end
    libro = RubyXL::Workbook.new
    libro.add_worksheet("Datos")
    hoja = libro.worksheets[0]
    hoja.insert_cell(0, 0, "Nombre y apellido")
    hoja.insert_cell(0, 1, "CUIL")
    hoja.insert_cell(0, 2, "Fecha nacimiento")
    @convocation.applications.each_with_index do |postulacion, i|
      hoja.insert_cell(i+1, 0, postulacion.applicant.apellidos_nombres)
      hoja.insert_cell(i+1, 1, postulacion.applicant.cuil)
      hoja.insert_cell(i+1, 2, postulacion.applicant.fecha_nacimiento.to_s)
    end
    file_name = "#{Rails.root}/tmp/post_#{@convocation.nombre}.xlsx"
    libro.write(file_name)
    @convocation.update(estado: Convocation::EVALUACION_MINISTERIO)
    send_file file_name
  end

  def html_prepostulantes_evaluados
    render 'carga_archivo_prepostulantes_evaluados'
  end

  def importar_prepostulantes_evaluados
    if params[:file]
      @convocation.importar_prepostulantes_evaluados(params[:file])
      render "applications/import_finished"    
    else
      render "applicants/import_fail"
    end
  end

  private

    def set_convocation
      @convocation = Convocation.find(params[:id])
    end

    def convocation_params
      params.require(:convocation).permit(:fecha_limite, :fecha_inicio, :nombre)
    end

    def resultados_as_json
      json = {}
      adjudicados = []
      rechazados = []
      @resultados[:adjudicados].each do |ad| 
        adjudicados << {
          nombre: ad.applicant.to_s,
          empresa: ad.organization.to_s
        }        
      end
      @resultados[:rechazados].each do |ad| 
        rechazados << {
          nombre: ad.applicant.to_s,
          empresa: ad.organization.to_s
        }        
      end
      json[:rechazados] = rechazados
      json[:adjudicados] = adjudicados
      json
    end
end

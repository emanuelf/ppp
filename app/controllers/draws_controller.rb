class DrawsController < ApplicationController
  before_action :set_draw, only: [:show, :edit, :update, :destroy]

  respond_to :html

  skip_before_filter :authenticate_user!, only: [:send_mails, :make_draw, :make!, :carta_beneficiario]
  skip_before_filter :verify_authenticity_token, only: [:send_mails, :make!, :make_draw, :carta_beneficiario]

  def index
    @draws = Draw.all
    respond_with(@draws)
  end

  def show
    respond_with(@draw)
  end

  def new
    @draw = Draw.new
    respond_with(@draw)
  end

  def edit
  end

  def create
    @draw = Draw.new(draw_params)
    @draw.fecha_realizacion = Date.parse(params[:fecha_realizacion])
    @draw.save
    respond_with(@draw)
  end

  def update
    @draw.fecha_realizacion = Date.parse(params[:fecha_realizacion])
    @draw.update(draw_params)
    respond_with(@draw)
  end

  def destroy
    @draw.destroy
    respond_with(@draw)
  end

  private
    def set_draw
      @draw = Draw.find(params[:id])
    end

    def draw_params
      params.require(:draw).permit(:fecha_realizacion, :nombre, :realizado, :realizado_el)
    end
end

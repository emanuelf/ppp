# == Schema Information
#
# Table name: applicants
#
#  id                           :integer          not null, primary key
#  apellidos                    :string(255)
#  nombres                      :string(255)
#  sexo                         :string(1)
#  cuil                         :string(255)
#  dni                          :string(255)
#  fecha_nacimiento             :date
#  telefono                     :string(30)
#  estado_civil                 :string(20)
#  hijos                        :integer
#  domicilio                    :string(255)
#  localidad                    :string(255)
#  departament_id               :integer
#  codigo_postal                :string(10)
#  escolaridad_nivel            :string(255)
#  escolaridad_cursa            :boolean
#  escolaridad_finalizada       :boolean
#  escolaridad_quiere_finalizar :boolean
#  fecha_solicitud              :date
#  email                        :string(255)
#  habilitado                   :boolean
#  contratado                   :boolean
#  created_at                   :datetime
#  updated_at                   :datetime
#

class Applicant < ActiveRecord::Base

  belongs_to :departament
  accepts_nested_attributes_for :departament
  
  validates_presence_of :apellidos, 
                        :nombres, 
                        :cuil, 
                        :dni, 
                        :sexo, 
                        :estado_civil, 
                        :fecha_solicitud,
                        :fecha_nacimiento

  def self.import(file)
    excluidos_por_edad = []
    workbook = RubyXL::Parser.parse(file.path)
    primero = true
    workbook[0].each do |row| 
      next if row.nil?
      if primero 
        primero = false
        next
      end
      attrs = {}
      attrs[:fecha_nacimiento] = row[5].value if row[5]     
      attrs[:apellidos]        = row[0].value if row[0]
      attrs[:nombres]          = row[1].value if row[1]
      attrs[:sexo]             = row[2].value.to_s[0] if row[2] #(M)ujer|(V)aron
      
      attrs[:cuil] = row[3].value if row[3]
      attrs[:cuil].gsub!("-","") if attrs[:cuil]
      
      attrs[:dni]                          = row[4].value if row[4]
      attrs[:telefono]                     = row[6].value if row[6]
      attrs[:estado_civil]                 = row[7].value if row[7]
      attrs[:hijos]                        = build_hijos(row[8].value) if row[8]
      attrs[:domicilio]                    = row[9].value if row[9]
      attrs[:localidad]                    = row[10].value if row[10]
      attrs[:departament]                  = Departament.find_by_nombre(row[11].value) if row[11]
      attrs[:codigo_postal]                = row[12].value if row[12]
      attrs[:escolaridad_nivel]            = row[13].value if row[13]
      attrs[:escolaridad_cursa]            = build_boolean(row[14])
      attrs[:escolaridad_finalizada]       = build_boolean(row[15])
      attrs[:escolaridad_quiere_finalizar] = build_boolean(row[16])
      attrs[:fecha_solicitud]              = row[17].value if row[17]
      attrs[:email]                        = row[18].value if row[18]
      
      next if attrs[:fecha_nacimiento].nil?
      
      fecha_nacimiento = attrs[:fecha_nacimiento].to_date
      edad = ((Date.today - fecha_nacimiento) / 365.25).to_i
      if edad < 16 || edad > 25
        attrs[:edad] = edad 
        excluidos_por_edad << attrs
      else
        a = Applicant.new(attrs)
        a.save
      end
    end
    
    excluidos_por_edad
  end

  def edad
    edad = ((Date.today - fecha_nacimiento) / 365.25).to_i
  end

  def self.build_hijos(hijos_string)
    return 0 if hijos_string.nil?
    hijos_string.gsub(/[^0-9]/, '').to_i
  end

  def important_data
    "#{apellidos}, #{nombres}, Dni: #{dni}"
  end

  def apellidos_nombres
    "#{apellidos}, #{nombres}"
  end

  def to_s
    important_data
  end

  def self.build_boolean row
    return nil if row.nil? || row.value.nil?
    return true if row.value.downcase == "si" 
    false
  end
end

  # == Schema Information
#
# Table name: convocations
#
#  id                  :integer          not null, primary key
#  fecha_limite        :date
#  fecha_inicio        :date
#  nombre              :string(50)
#  created_at          :datetime
#  updated_at          :datetime
#  sorteo_realizado_el :date
#

class Convocation < ActiveRecord::Base

  has_many :applications
  accepts_nested_attributes_for :applications 

  has_many :convocation_organizations
  has_many :organizations, through: :convocation_organizations

  validates_presence_of :nombre, :fecha_inicio, :fecha_limite

  ABIERTA               = "ABIERTA"
  EVALUACION_MINISTERIO = "EVALUACION_MINISTERIO"
  EVALUADA              = "EVALUADA"
  CERRADA               = "CERRADA"

  ESTADOS = [ABIERTA, EVALUACION_MINISTERIO, EVALUADA, CERRADA]

   def sortear!
    adjudicados = []
    rechazados = []
    Convocation.transaction do    
      organizations.order(cantidad_personal: :desc).each do |org| 
        vacantes = org.vacantes
        #busco las postulaciones a esta empresa
        post_en_esta_org = applications.where(organization: org, 
                                              estado: Application::ACEPTADO)
                                        .shuffle
                                        .shuffle
        vacantes.times do |i|
          postulacion = post_en_esta_org[i]
          #en el hipotético caso en el que hayan más vacantes que postulaciones
          #la siguiente postulacion puede no existir
          if postulacion
            #byebug
            postulacion.update(estado: Application::ASIGNADO)
            adjudicados << postulacion
          end
        end
      end
      adjudicados_ids = adjudicados.map { |a| a.id }
      rechazados = applications.where.not(id: adjudicados_ids)
      rechazados.each { |r| r.update(estado: Application::NO_ASIGNADO) }
      update(estado: CERRADA, sorteo_realizado_el: Date.today)
    end
    {adjudicados: adjudicados, rechazados: rechazados}
  end

  def results
    adjudicados = []
    rechazados = []
    applications.each do |a|
      adjudicados << a if a.adjudicado?
      rechazados << a if a.no_asignado?
    end
    {adjudicados: adjudicados, rechazados: rechazados}
  end

  def to_s
    "#{nombre}. Fecha inicio: #{fecha_inicio}, fecha límite: #{fecha_limite}"
  end

  def important_data
    "#{nombre}"
  end

  def algun_beneficiario_asignado?(organization)
    results[:adjudicados].each do |adj|
      return true if adj.organization == organization
    end
    false
  end

  def adjudicados_of(organization)
    adjs = []
    results[:adjudicados].each do |adj|
      adjs << adj if adj.organization == organization
    end
    adjs
  end

  def importar_prepostulantes_evaluados(excel)
    workbook = RubyXL::Parser.parse(excel.path)
    primero = true
    workbook[0].each do |row|
      next if row.nil?
      if primero 
        primero = false
        next
      end
      application = Application.joins(:applicant).where(applicants: {cuil: row[1].value.gsub("-","")}).take
      if application
        application.set_estado_desde_codigo(row[3].value.to_i)
        application.save!
      end
    end
    update(estado: EVALUADA)
  end

  def empresas_con_adjudicaciones
    organizations.select {|o| algun_beneficiario_asignado?(o)}
  end

  def sorteo_realizado?
    sorteo_realizado_el  
  end


  def estado_to_human
    case estado
    when EVALUACION_MINISTERIO  
      "En evaluación por el ministerio"
    else
      estado.capitalize unless estado.nil?
    end
  end

  def cerrada?
    estado == CERRADA
  end

end

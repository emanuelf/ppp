# == Schema Information
#
# Table name: departaments
#
#  id         :integer          not null, primary key
#  nombre     :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Departament < ActiveRecord::Base
  def important_data
    "#{nombre}"
  end
end

# == Schema Information
#
# Table name: draws
#
#  id                :integer          not null, primary key
#  fecha_realizacion :date
#  nombre            :string(255)
#  realizado         :boolean
#  realizado_el      :time
#  created_at        :datetime
#  updated_at        :datetime
#

class Draw < ActiveRecord::Base

  has_many :applications

  validates_presence_of :nombre, :fecha_realizacion

  def sortear!
    empresas = Organization.all
    adjudicados = []
    rechazados = []
    Draw.transaction do    
      empresas.each do |org| 
        vacantes = org.vacantes
        #busco las postulaciones a esta empresa
        postulaciones_a_esta_empresa = Application.where(organization: org, 
                                                         estado: Application::PENDIENTE)
                                                  .shuffle.shuffle
        vacantes.times do |i|
          postulacion = postulaciones_a_esta_empresa[i]
          #en el hipotético caso en el que hayan más vacantes que postulaciones
          #la siguiente postulacion puede no existir
          if postulacion
            #byebug
            postulacion.update(estado: Application::ADJUDICADO)
            adjudicados << postulacion
          end
        end
      end
      adjudicados_ids = adjudicados.map { |a| a.id }
      rechazados = applications.where.not(id: adjudicados_ids)
      rechazados.each { |r| r.update(estado: Application::RECHAZADO) }
      update(realizado: true, realizado_el: Time.now)
    end
    {adjudicados: adjudicados, rechazados: rechazados}
  end

  def results
    adjudicados = []
    rechazados = []
    applications.each do |a|
      adjudicados << a if a.adjudicado?
      rechazados << a if a.no_asignado?
    end
    {adjudicados: adjudicados, rechazados: rechazados}
  end

end

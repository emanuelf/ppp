# == Schema Information
#
# Table name: applications
#
#  id                :integer          not null, primary key
#  applicant_id      :integer
#  organization_id   :integer
#  tipo_contratacion :string(30)
#  created_at        :datetime
#  updated_at        :datetime
#  estado            :string(20)
#  draw_id           :integer
#  convocation_id    :integer
#

class Application < ActiveRecord::Base
  
  PREPOSTULADO = "PREPOSTULADO"
  RECHAZADO = "RECHAZADO"
  EN_EVALUACION = "EN_EVALUACION"
  ACEPTADO = "ACEPTADO"
  ASIGNADO = "ASIGNADO"
  NO_ASIGNADO = "NO_ASIGNADO"

  ESTADOS = [PREPOSTULADO, RECHAZADO, EN_EVALUACION, ACEPTADO, ASIGNADO, NO_ASIGNADO]

  belongs_to :applicant
  belongs_to :organization
  belongs_to :draw
  belongs_to :convocation

  validates_presence_of :applicant_id, :organization_id, :convocation_id, :estado


  validates_inclusion_of :estado, 
                         :in => ESTADOS, 
                         :message => "%s No es un estado válido"

  ENTRENAMIENTO = "ENTRENAMIENTO"
  CTI = "CTI"

  def adjudicado?
    estado.eql? ASIGNADO
  end

  def rechazado?
    estado.eql? RECHAZADO
  end

  def no_asignado?
    estado.eql? NO_ASIGNADO
  end

  def set_estado_desde_codigo(codigo)
    case codigo
    when 0..4
      self.estado = RECHAZADO
    when 5
      self.estado = ACEPTADO
    end
  end

  def self.find_applicant (cuil) 
    @postulante = Applicant.find_by cuil: cuil.to_s
    if @postulante
      @postulante.id
    else
      nil
    end
  end

  def self.find_organization (cuit) 
    @organization = Organization.find_by cuit: cuit.to_s
    if @organization
      @organization.id
    else
      nil
    end
  end

  def self.find_departament (nombre) 
    @departament = Departament.find_by nombre: nombre.to_s
    if @departament
      @departament.id
    else
      nil
    end
  end

  def self.import(file, convocation)
    workbook = RubyXL::Parser.parse(file.path)
    primero = true
    workbook[0].each do |row|
      next if row.nil?
      if primero 
        primero = false
        next
      end
      attrs = {}
      if row[0] #Buscar si existe y si ya se cargo la postulacion.
        attrs[:applicant_id] = find_applicant(row[0].value)
      end 
      if row[2]
        #Buscar si existe
        organization = find_organization(row[2].value)
        if organization
          attrs[:organization_id] = organization
        else
          #Create organization
          attrs_org = {}
          attrs_org[:nombre]            = row[1].value if row[1] 
          attrs_org[:cuit]              = row[2].value if row[2] 
          attrs_org[:codigo_actividad]  = row[3].value if row[3]
          attrs_org[:cantidad_personal] = row[4].value if row[4]
          attrs_org[:domicilio]         = row[5].value if row[5]
          attrs_org[:localidad]         = row[6].value if row[6]
          #attrs_org[:departamento]     = find_departament(row[7].value) if row[7]
          attrs_org[:telefono]          = row[8].value if row[8]
          attrs_org[:fax]               = row[9].value if row[9]
          attrs_org[:email]             = row[10].value if row[10]
          org = Organization.new(attrs_org)
          org.save
          attrs[:organization_id] = org.id
        end
        convocation.organizations << Organization.find(attrs[:organization_id])
        convocation.save
      end 
      attrs[:tipo_contratacion] = row[11].value if row[11]
      attrs[:estado]            = PREPOSTULADO
      attrs[:convocation_id]    = convocation.id
      #byebug
      a = Application.new(attrs)
      a.save
    end
  end

  def to_s
    "#{applicant.apellidos}, #{applicant.nombres}"
  end


  def estado_to_human
    case estado
    when EN_EVALUACION  
      "En evaluación"
    else
      estado.gsub("_"," ").capitalize
    end
  end
end

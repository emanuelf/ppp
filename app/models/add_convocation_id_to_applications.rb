class AddConvocationIdToApplications < ActiveRecord::Base
  def change
    add_reference :applications, :convocation, index: true
  end
end

# == Schema Information
#
# Table name: organizations
#
#  id                :integer          not null, primary key
#  cuit              :string(255)
#  codigo_actividad  :string(15)
#  cantidad_personal :integer
#  calle             :string(255)
#  nro               :string(255)
#  localidad         :string(255)
#  telefono          :string(255)
#  fax               :string(255)
#  email             :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#  nombre            :string(255)
#  departament_id    :integer
#  domicilio         :string(255)
#

class Organization < ActiveRecord::Base
  has_many :applications
  accepts_nested_attributes_for :applications 
  
  belongs_to :departament
  
  validates_presence_of :nombre, :cuit, :codigo_actividad, :cantidad_personal, :email
  validates_uniqueness_of :cuit

  def important_data
    "#{nombre}, CUIT: #{cuit}"
  end

  def vacantes
    return 1 if cantidad_personal.zero?
    cantidad_personal*2
  end

  def to_s
    important_data
  end
end

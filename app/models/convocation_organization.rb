# == Schema Information
#
# Table name: convocation_organizations
#
#  id              :integer          not null, primary key
#  convocation_id  :integer
#  organization_id :integer
#  created_at      :datetime
#  updated_at      :datetime
#

class ConvocationOrganization < ActiveRecord::Base
  belongs_to :convocation
  belongs_to :organization
end

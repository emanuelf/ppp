json.extract! application, :id, :applicant_id, :organization_id, :tipo_contratacion, :created_at, :updated_at
json.url application_url(application, format: :json)
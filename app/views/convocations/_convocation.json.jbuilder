json.extract! convocation, :id, :fecha_limite, :fecha_inicio, :nombre, :created_at, :updated_at
json.url convocation_url(convocation, format: :json)
class DrawMailer < ActionMailer::Base
  
  default from: ENV['SENDGRID_USERNAME']

  def mail_adjudicado(application)
    @application = application
    to = ENV['TEST_MAIL']            if Rails.env.development? 
    to = ENV['TEST_MAIL'] || application.applicant.email if Rails.env.production?
    mail(
      to: to, 
      subject: 'Su postulación para el programa PETEIHA TRABAJO ha sido seleccionada',
      layout: 'beneficiario')
  end

  def mail_empresas(org, adjudicados, convocation)
    byebug
    @org         = org
    @adjudicados = adjudicados
    @convocation = convocation
    to = ENV['TEST_MAIL'] if Rails.env.development? 
    to = ENV['TEST_MAIL'] || @org.email if Rails.env.production?
    mail(
      to: to,
      subject: 'Adjudicados por el programa PETEIHA',
      layout: 'empresa'
    )
  end
end
